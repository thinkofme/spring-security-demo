# Spring-security
  为了给MIS系统添加一套较为通用的权限控制功能，本项目基于Spring，整合Spring的security模块，实现用户管理和权限控制，主要内容如下：
  
  1.登录，包括“记住我”的功能；
  
  2.加密，存储的密码不采用明文,初始密码123；
  
  3.拦截器：对静态文件（HTML/JS/CSS等）进行权限控制，无权限则请求不到；
  
  4.后台接口权限控制：对后台接口启用权限控制，对应的接口若不满足权限或角色要求，则请求失败，使用@Secured实现；
  
  5.用户-角色-权限使用常规RBAC的模型，用户到角色，角色到权限均为多对多关系映射；


![输入图片说明](http://git.oschina.net/uploads/images/2017/0322/151628_4dd347da_1110335.jpeg "在这里输入图片标题")
  
  效果图：
 
![输入图片说明](https://git.oschina.net/uploads/images/2017/0809/165717_a8d350ef_1110335.png "下载.png")
![输入图片说明](http://git.oschina.net/uploads/images/2017/0209/144409_b7aaea2f_1110335.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2017/0209/144419_70c4b9f4_1110335.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2017/0323/114134_1c478e5f_1110335.png "在这里输入图片标题")

### 附录：个人作品索引目录（持续更新）

#### 基本篇

1. [SpringMVC,Mybatis,Spring三大框架的整合实现增删改查](https://gitee.com/shenzhanwang/SSM)
2. [Struts2,Hibernate,Spring三大框架的整合实现增删改查](https://gitee.com/shenzhanwang/S2SH)
3. [Spring,SpringMVC和Hibernate的整合实现增删改查](https://gitee.com/shenzhanwang/SSH)
4. [Spring平台整合activiti工作流引擎实现OA开发](https://gitee.com/shenzhanwang/Spring-activiti)
5. [Spring发布与调用REST风格的WebService](https://gitee.com/shenzhanwang/Spring-REST)
6. [Spring整合Apache Shiro框架，实现用户管理和权限控制](https://gitee.com/shenzhanwang/Spring-shiro)
7. [使用Spring security做权限控制](https://gitee.com/shenzhanwang/spring-security-demo)

#### 中级篇

8. [Spring连接mongoDB数据库实现增删改查](https://gitee.com/shenzhanwang/Spring-mongoDB)
9. [Spring连接Redis实现缓存](https://gitee.com/shenzhanwang/Spring-redis)
10. [Spring连接图存数据库Neo4j实现增删改查](https://gitee.com/shenzhanwang/Spring-neo4j)
11. [Spring平台整合消息队列ActiveMQ实现发布订阅、生产者消费者模型（JMS）](https://gitee.com/shenzhanwang/Spring-activeMQ)
12. [Spring整合消息队列RabbitMQ实现四种消息模式（AMQP）](https://gitee.com/shenzhanwang/Spring-rabbitMQ)
13. Spring整合Jasig CAS框架实现单点登录（未开源）
14. Spring框架的session模块实现集中式session管理（未开源）
15. [Spring整合websocket实现即时通讯](https://gitee.com/shenzhanwang/Spring-websocket)
16. 使用Spring boot整合mybatis,rabbitmq,redis,mongodb实现增删改查（未开源）
17. [Spring MVC整合FastDFS客户端实现文件上传](https://gitee.com/shenzhanwang/Spring-fastdfs)

#### 高级篇

18. 搭建zookeeper集群提供目录服务（未开源）
19. 使用ubuntu+apache+SVN+SVNadmin+maven+Nexus+Hudson搭建持续集成环境（未开源）
20. Spring框架整合dubbo框架实现分布式服务治理（SOA架构）（未开源）
21. Spring框架整合dubbox实现微服务架构（MSA架构）（未开源）
22. 使用Spring Cloud实现微服务架构（MSA架构）（未开源）
23. 使用FastDFS搭建分布式文件系统（高可用、负载均衡）（未开源）
24. 搭建高可用nginx集群和Tomcat负载均衡（未开源）
25. 搭建可扩展的ActiveMQ高可用集群（未开源）
26. 实现Mysql数据库的主从复制、读写分离、分表分库、负载均衡和高可用（未开源）
27. 搭建高可用redis集群实现分布式缓存（未开源）
28. [Spring整合SolrJ实现全文检索](https://gitee.com/shenzhanwang/Spring-solr)

### 捐赠，私信索取未开源代码
![输入图片说明](https://gitee.com/uploads/images/2017/1217/145453_b639d3db_1110335.png "mm_facetoface_collect_qrcode_1513493342741.png")

![输入图片说明](https://gitee.com/uploads/images/2017/1217/145502_2d4fe513_1110335.jpeg "1513493369523.jpg")
